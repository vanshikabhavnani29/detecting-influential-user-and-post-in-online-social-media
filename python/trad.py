


import pandas as pd
import numpy as np

# text preprocessing
from nltk import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import re

# plots and metrics
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix

# feature extraction / vectorization
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

# classifiers
# from sklearn.naive_bayes import MultinomialNB
# from sklearn.linear_model import LogisticRegression, SGDClassifier
# from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline

# save and load a file
import pickle




df_train = pd.read_csv('data_train.csv')
df_test = pd.read_csv('data_test.csv')

X_train = df_train.Text
X_test = df_test.Text

y_train = df_train.Emotion
y_test = df_test.Emotion

class_names = ['joy', 'sadness', 'anger', 'neutral', 'fear']
data = pd.concat([df_train, df_test])

print('size of training set: %s' % (len(df_train['Text'])))
print('size of validation set: %s' % (len(df_test['Text'])))
print(data.Emotion.value_counts())

data.head()


def preprocess_and_tokenize(data):    

    #remove html markup
    data = re.sub("(<.*?>)", "", data)

    #remove urls
    data = re.sub(r'http\S+', '', data)
    
    #remove hashtags and @names
    data= re.sub(r"(#[\d\w\.]+)", '', data)
    data= re.sub(r"(@[\d\w\.]+)", '', data)

    #remove punctuation and non-ascii digits
    data = re.sub("(\\W|\\d)", " ", data)
    
    #remove whitespace
    data = data.strip()
    
    # tokenization with nltk
    data = word_tokenize(data)
    
    # stemming with nltk
    porter = PorterStemmer()
    stem_data = [porter.stem(word) for word in data]
        
    return stem_data



import nltk
nltk.download('punkt')
# TFIDF, unigrams and bigrams
vect = TfidfVectorizer(tokenizer=preprocess_and_tokenize, sublinear_tf=True, norm='l2', ngram_range=(1, 2))

# fit on our complete corpus
vect.fit_transform(data.Text)

# transform testing and training datasets to vectors
X_train_vect = vect.transform(X_train)
X_test_vect = vect.transform(X_test)




svc = LinearSVC(tol=1e-05)
svc.fit(X_train_vect, y_train)

ysvm_pred = svc.predict(X_test_vect)

# print("Accuracy: {:.2f}%".format(accuracy_score(y_test, ysvm_pred) * 100))
# print("\nF1 Score: {:.2f}".format(f1_score(y_test, ysvm_pred, average='micro') * 100))
# print("\nCOnfusion Matrix:\n", confusion_matrix(y_test, ysvm_pred))

# plot_confusion_matrix(y_test, ysvm_pred, classes=class_names, normalize=True, title='Normalized confusion matrix')
# plt.show()



#Create pipeline with our tf-idf vectorizer and LinearSVC model
svm_model = Pipeline([
    ('tfidf', vect),
    ('clf', svc),
])




# save the model
filename = 'tfidf_svm.sav'
pickle.dump(svm_model, open(filename, 'wb'))



model = pickle.load(open(filename, 'rb'))



###senti 

toptweets=['Hope your day was awesome! This is just me handling a #zoom call and drinking some #nespresso. #kerimaletto #coffee #spring #hello https://t.co/FNNspLSdFU', 'There, my classmates are great and funny. I enjoyed my times for 1 semester and even found myself a partner in my class. Cute, sweet. My gang and 2 bestfriends; rough, funny and sweet.\n\n-To be continued-\n#shortstory \n#hello \n#thanks', 'The Return! https://t.co/SG6NeBaHjs\n#twitch\n#comewatch \n#doctor\n#pc\n#gaming \n#itstime\n#return\n#Hello! \n#gamer \n#yay', "Sorry for the inconvenience. #payphone #alsoasis #westriver #bisonburger #hello #tourism #southdakota @ Al's Oasis https://t.co/16qVGEWmkT", '@ollielupinn #hello I want']


encoding = {
    'joy': 0,
    'fear': 1,
    'anger': 2,
    'sadness': 3,
    'neutral': 4
}

sentilist=[]
sentlist=[]
sentivalue=[]
for i in range(len(toptweets)): 
   

    predictt=model.predict([toptweets[i]])
  
    sentlist.append(predictt)
    a=sentlist[i]
    a=a.tolist()
  
    a=a[0]
    sentilist.append(a)
   
    sentivalue.append(encoding[a])
   
print("sentilist",sentilist)
print("sentivalue",sentivalue)