$(document).ready(function(){ 
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 150) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});






const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");
const textArray = ["Hi!"," Welcome to Influencia!;)","I am sure you would have a great network!", "Enjoy :)"];
const typingDelay = 100;
const erasingDelay = 100;
const newTextDelay = 1550; // Delay between current and next text like before erasing the text
let textArrayIndex = 0;
let charIndex = 0;

function type() {
    if (charIndex < textArray[textArrayIndex].length) {
      if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
      typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
      charIndex++;
      setTimeout(type, typingDelay);
    } 
    else {
      cursorSpan.classList.remove("typing");
        setTimeout(erase, newTextDelay);
    }
  }
  
  function erase() {
      if (charIndex > 0) {
      if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
      typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex-1);
      charIndex--;
      setTimeout(erase, erasingDelay);
    } 
    else {
      cursorSpan.classList.remove("typing");
      textArrayIndex++;
      if(textArrayIndex>=textArray.length) textArrayIndex=0;
      setTimeout(type, typingDelay + 1100);
    }
  }
  
  document.addEventListener("DOMContentLoaded", function() { // On DOM Load initiate the effect
    if(textArray.length) setTimeout(type, newTextDelay + 250);
  });



  
  let faqs = [
    {
      question: 'What is influential user?',
      answer: 'Influential users are the ones who create an impact on its audience, be it positive or negative. ', open: false
    },
    {
      question: 'What is influential tweet?',
      answer: 'Influential tweet is the most spreaded tweet across the network. It is not necessary that an Influential tweet will belong to influential user, these two are not related.', open: false
    },
    {
        question: "What is sentiment analysis?",
        answer: 'It is the process of determining the emotional tone behind a series of words, used to gain an understanding of the the attitudes, opinions and emotions expressed within an online mention.',   open: false
      },
    {
      question: "Why to determine influentiality?",
      answer: 'Influentiality is determined so that we can know who is popular among people around the globe and then promote the positive influencers\'  views to the world.',  open: false
    },
    {
        question: "What is the outcome of sentiment analysis?",
        answer: 'The outcome of sentiment analysis of top 5 tweets is amongst the five emotions like Joy, Fear, Anger, Sadness and Neutral which has labelling from value 0 to  4 respectively. ',open:false },
    {
      question: "How the website works?",
      answer: 'In this website, after the authenticated user provides a hashtag, the tweets related to that hashtag is extracted for determining the influential tweet and user. Along with this, top 5 tweets and users are also determined which can be viewed by selecting the respective option on the sidebar.  The sentiments of top 5 tweets are also depicted in the Sentiment Analysis category.',   open: false
    },
    {
      question: "Can anyone login to see influentiality?",
      answer: 'Anyone with a public Twitter account can login and specify the username of the account to get the influentiality.',   open: false
    },
    {
      question: "What is TII?",
      answer: 'TII also known as Tweet Influence Index focuses on the features extracted from users\' tweets. This index is created upon the features included in the tweet (number of hastags, mentions, url, length and time of tweet) and the ones shaped by the users reading it (number of retweets, favourites and its similarity to ther tweets). It aims at ranking the tweets according to the influence they spread.',   open: false
    },
    {
      question: "What is GII?",
      answer: 'GII also known as General Influence Index is deployed to rank the most influential users based on not only their own characteristics but also their tweets influence. it can also be denoted as the sum of TII and UII. UII also known as User Influence Index is based on the attributes introduced for users like number of followers, following, favourites, tweet count etc.',   open: false
    }
  ]
  
  const faqs_element = document.querySelector('.faqs');
  
  faqs.map(function (faq) {
    const faq_element = document.createElement('div');
    faq_element.classList.add('faq');
    if (faq.open) {
      faq_element.classList.add('open');
    }
  
    faq_element.addEventListener('click', function (e) {
      faq.open = true;
      this.classList.toggle('open');
    });
  
    const faq_question_element = document.createElement('div');
    faq_question_element.classList.add('question');
    faq_question_element.innerText = faq.question;
  
    faq_element.appendChild(faq_question_element);
  
    const faq_answer_element = document.createElement('div') ;
    faq_answer_element.classList.add('answer');
    faq_answer_element.innerText = faq.answer;
  
    faq_element.appendChild(faq_answer_element);
  
    faqs_element.appendChild(faq_element);
  });




  function topbuttonhover(){
    document.getElementById("scroll").style.backgroundColor="maroon";
    document.getElementById('scroll').firstChild.style.borderBottomColor="rgb(255,214,214)";

  }
  function topbuttonout(){
    document.getElementById("scroll").style.backgroundColor="rgb(255,214,214)";
    document.getElementById('scroll').firstChild.style.borderBottomColor="maroon";

    
  }