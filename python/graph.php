<?php
session_start();
$conn = mysqli_connect("localhost", "vanshika", "vanshik@143", "twitter");
// Check connection
if($conn === false){
  die("ERROR: Could not connect. " . mysqli_connect_error());
}
$data_points = array();
$tweets = array();    
$result = mysqli_query($conn, "SELECT * from tweetcsv ORDER BY id DESC");
$i = 10;
while($row = mysqli_fetch_array($result))
{        
    $point = array("x" => $i, "y" => $row['tii'],"label" => $row['user']);
    $ind = array("tweet" => $row['tweet']);
    $i += 10;
    array_push($tweets,$ind);
    array_push($data_points, $point);        
}
//print_r($data_points);
$data_points1 = array();
$result1 = mysqli_query($conn, "SELECT * from usercsv");
$sum = mysqli_query($conn, "SELECT SUM(gii) from usercsv");
$res_sum = mysqli_fetch_array($sum);
// echo($res_sum[0]);
$i = 10;
while($row = mysqli_fetch_array($result1))
{        
    $point1 = array("x" => $i, "y" => $row['gii'],"legendText" => $row['user'], "indexLabel" => $row['user']);
    $i += 10;
    array_push($data_points1, $point1);        
}

$data_points2 = array();
$result2 = mysqli_query($conn, "SELECT * from tweetcsv");
$i = 10;
while($row = mysqli_fetch_array($result2))
{        
    $point2 = array("x" => $i, "y" => $row['polarity'], "label" => $row['user']);
    $i += 10;
    //print_r($point2);
    array_push($data_points2, $point2);        
}

$data_points3 = array();
$res_max_arr = array();
$res_min_arr = array();
$result3 = mysqli_query($conn, "SELECT * from tweetcsv");
$res = mysqli_query($conn, "SELECT MAX(gii) from usercsv");
while($row = mysqli_fetch_array($res)){
  //print_r($row);
  $arr = array("max" => $row[0]);
  array_push($res_max_arr,$arr);
}
$res = mysqli_query($conn, "SELECT MIN(gii) from usercsv");
while($row = mysqli_fetch_array($res)){
  //print_r($row);
  $arr = array("min" => $row[0]);
  array_push($res_min_arr,$arr);
}
//print_r($res_min_arr);
// echo($res_min_arr[0]["min"]);
// echo"\n";
// echo($res_max_arr[0]["max"]);
$i = 10;
while($row = mysqli_fetch_array($result3))
{        
    $point3 = array("x" => $i, "label" => $row['user']);
    $i += 10;
    array_push($data_points3, $point3);        
}
// $sql = "SELECT tii,polarity,tweet from tweetcsv";          
// $result = mysqli_query($conn,$sql);

// $sql1 = "SELECT gii from usercsv";          
// $result1 = mysqli_query($conn,$sql1);
//mysqli_close($conn);
?>
<!DOCTYPE html>
<html lang="en">
<head>
        <title>Statistics</title>
        <!--Meta tag-->
        <link rel = "icon" href = "i.svg" type = "image/x-icon" width="200px">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Including the bootstrap CDN -->
        <link rel="stylesheet" href= "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
        <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
        <script src= "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"> </script> 
        <script src= "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> 
        <!--Including style sheet-->
        <link rel="stylesheet" href="style.css">
        <!--Google fonts-->
        <link href="https://fonts.googleapis.com/css2?family=Ranchers&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Courgette&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Permanent+Marker&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Sonsie+One&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2:wght@500&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2:wght@800&display=swap" rel="stylesheet">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!--Icon Of Page-->
        <link rel="icon" href="./images/icon2.JPG">
        <!--Google Icons-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdn.anychart.com/releases/8.0.0/js/anychart-base.min.js"></script>
        <script src="profilepage.js"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </head>
    <style>
a{
color:maroon;
text-decoration:none;
}
.modal-dialog-centered::before {
    display: block;
    /*height: calc(100vh - 1rem);*/
    /*content: "";*/
}
.senti:hover{
  font-size: larger;
  text-decoration: underline;
}
.modal-dialog {
    position: absolute;
    width: 100%;
    top: 125px;
    left: 430px;
    pointer-events: none;
}
.modal-content {
    background-color: #fff;
    margin: auto;
    padding: 20px;
    /* border: 1px solid #888; */
    width: 80%;
}
.modal {
    position: fixed;
    top: -300px;
    left: 0;
    z-index: 1050;
    display: none;
    width: 100%;
    height: 100%;
    overflow: hidden;
    background-color: transparent;
    /* outline: 0; */
}
.dashboard{
  cursor: pointer;
}
.col-sm-3 {
    max-width: 26%;
}
.footer-header {
    padding: 10px 50px;
}
.dash:hover {
    font-size: larger;
    text-decoration: underline;
}
.stats:hover {
    font-size: larger;
    text-decoration: underline;
}
.stats{
    text-decoration: underline;  
}
.tweet:hover {
    font-size: larger;
    text-decoration: underline;
}
.user:hover {
    font-size: larger;
    text-decoration: underline;
}
.profile form input{
padding: 3px 20px;
width:60%;
border:none;
outline:none;
border:2px solid pink;
align-items:center;
justify-content: center;
margin:auto;
}
.profile form input:focus{
    border:2px solid maroon;
    outline:none;
}
a:hover{
    color:maroon;
    text-decoration:none;
}
.is-invalid{
    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='12' height='12' fill='none' stroke='%23dc3545' viewBox='0 0 12 12'%3e%3ccircle cx='6' cy='6' r='4.5'/%3e%3cpath stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/%3e%3ccircle cx='6' cy='8.2' r='.6' fill='%23dc3545' stroke='none'/%3e%3c/svg%3e");
    background-repeat: no-repeat;
    background-position: right calc(.375em + .1875rem) center;
    background-size: calc(.75em + .375rem) calc(.75em + .375rem);
}
*::selection{
    background:rgb(149, 42, 42);
    color:white;
}
.divider{
    width:20px;
    height:auto;
    display:inline-block;
}
.wrapper {
  width: 100vw;
  height: 100vh;
  background: whitesmoke;
}
.flex-center {
  display: flex;
  justify-content: center;
  align-items: center;
}
.container {
  width: 10em;
  height: 10em;
  position: relative;
  background: pink;
  border-radius: 50%;
}
.container .dot {
  background: darkred;
  content: "";
  border-radius: 0.5em;
  margin-top: -0.5em;
  margin-left: auto;
  margin-right: auto;
  width: 1em;
  height: 1em;
  animation-name: bounce;
  animation-duration: 3000ms;
  animation-iteration-count: infinite;
  transition-timing-function: cubic-bezier(0, 0.99, 0, 0.1);
}
.button {
  background-color: darkred;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  border-radius: 50%;
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size:small;
  margin-right:1px;
  position:left;

}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(.even) {
  background-color: #dddddd;
}


.main {
  margin-left: 30px; /* Same as the width of the sidenav */
  font-size: 28px; /* Increased text to enable scrolling */
  padding: 0px 10px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
.container-dot {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
}
.dot-a {
  transform: rotate(0deg);
}
.dot-a .dot {
  animation-delay: 0ms;
}
.dot-b {
  transform: rotate(-22.5deg);
}
.dot-b .dot {
  animation-delay: -187.5ms;
}
.dot-c {
  transform: rotate(-45deg);
}
.dot-c .dot {
  animation-delay: -375ms;
}
.dot-d {
  transform: rotate(-67.5deg);
}
.dot-d .dot {
  animation-delay: -562.5ms;
}
.dot-e {
  transform: rotate(-90deg);
}
.dot-e .dot {
  animation-delay: -750ms;
}
.dot-f {
  transform: rotate(-112.5deg);
}
.dot-f .dot {
  animation-delay: -937.5ms;
}
.dot-g {
  transform: rotate(-135deg);
}
.dot-g .dot {
  animation-delay: -1125ms;
}
.dot-h {
  transform: rotate(-157.5deg);
}
.dot-h .dot {
  animation-delay: -1312.5ms;
}
@keyframes bounce {
  0%, 100% {
    transform: translatey(0);
 }
  50% {
    transform: translatey(10em);
 }
}

</style>

<body onload="loader()">
<div id="loading">
    <div class="wrapper flex-center">
        <div class="container">
            <div class="container-dot dot-a">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-b">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-c">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-d">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-e">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-f">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-g">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-h">
            <div class="dot"></div>
            </div>
        </div>
    </div>
</div>
<div class="mymaindiv" style="display:none;">
<div class="container-fluid">
  <nav class="navbar navbar-expand-sm  navbar-toggler static-top navbar-light" style="background-color:transparent; background-color:white; opacity:0.86;z-index:12; padding: 0.25rem 1.5rem;"> 
        <img src="i.svg" height="45px" class="d-inline-block align-top" alt="" loading="lazy">&nbsp;
        <div class="navbar-brand font-weight-bold" style="color:maroon;font-family: 'Courgette', cursive;font-size:xx-large;cursor: pointer" onclick="location.href = 'http://localhost:8080/home.php'">
            Influencia
        </div>
        <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02" style="float:right;">
            <ul class="navbar-nav ml-auto" style="font-size:small; color:pink;"> 
                <li class="nav-item"> 
                    <a class="nav-link" style="cursor: pointer;" onclick="logout" href="login.php">
                      <div style="padding-left:15px;">
                          <i class="material-icons" style="font-size: x-large;text-align:center;">
                              exit_to_app
                          </i>
                      </div>
                          Log out
                      </a> 
                </li>               
            </ul> 
        </div>
    </nav>
        
   
    <div class="row">
        <div class="col" style=" max-width:25%;height: 300px;">
            <div class="profileitemdiv" style="background-color:rgb(255, 220, 226); height: 1285px;color:maroon;  padding:20px;margin-top: 20px;border-radius: 20px;">         
                  
                  <div class="dashboard tweet" style="margin:10px " onclick="location.href = 'http://localhost:8080/tweets.php'">
                 
                 <p style=" padding-left:20px;"><b>Influential Tweets</b></p>
                 </div><hr>
                  <div class="dashboard user" style="margin:10px " onclick="location.href = 'http://localhost:8080/users.php'">
                 
                 <p style=" padding-left:20px;"><b>Infulential Users</b></p>
                 </div><hr>
                 <div class="dashboard senti" style="margin:10px " onclick="location.href = 'http://localhost:8080/sentiment.php'">
                 
                 <p style=" padding-left:20px;"><b>Sentiment Analysis</b></p>
                 </div><hr>

                 <div class="dashboard stats" style="margin:10px " onclick="location.href = 'http://localhost:8080/graph.php'">
                 
                 <p style="padding-left:20px;"><b>Statistics</b></p>
                 </div><hr>
             </div>  
        </div>
       <div class="main" style="display:inline;min-width:75%;">
              <div class="headings" style="padding:20px; ">
               
              <div id="chartContainer1" style="width: 45%; height: 300px;display: inline-block;"></div> 
              <!-- <span>1. if Polarity = 0 then sentiment is neutral</span> -->
              <div style="margin:120px"></div>
              <!-- <div id="chartContainer2" style="width: 45%; height: 300px;display: block;"></div><br> -->
              <div style="margin:120px"></div>
              <div id="chartContainer3" style="width: 45%; height: 300px;display: block;"></div>
              <div style="margin:120px"></div>

              <div id="chartContainer4" style="width: 45%; height: 300px;display: inline-block; "></div> 

              <div style=" display:inline-block; margin-left: 100px; width:40%;">
              <table style="margin-top:50px;width:50%;margin-left:100px;">
                <tr >
                  <th>range</th>
                  <th>Emotion</th>
                </tr>
                <tr >
                  <td >0</td>
                  <td >Joy</td>
                </tr>
                <tr >
                  <td >1</td>
                  <td>fear</td>
                </tr>
                <tr>
                  <td >2</td>
                  <td>anger</td>
                </tr>
                <tr>
                  <td >3</td>
                  <td>sadness</td>
                </tr>
                <tr>
                  <td >4</td>
                  <td>neutral</td>
                </tr>
              </table> 
              </div> 
            </div>
          </div>
    </div>
    <br>
    <br>
    <br>
    <br>
      <br>
  <footer style="background-color:  rgb(75, 39, 50);display: block;">
            
  <div class="footer contianer-fluid">
      <div class="footer-header">
          
          <h2 style="cursor: pointer" onclick="location.href = 'http://localhost:8080/home.php'">Influencia</h2>
      </div>
      <div class="row">
          <div class="col-sm-3">
              <ul type="none">
                  <lh><b>CUSTOMER CARE</b></lh>
                  <li>Contact Us</li>
                  <!-- <li>Return Order</li>
                  <li>Cancel Order</li> -->
                  <li>FAQ's</li>
              </ul>
          </div>
          <div class="col-sm-3">
              <ul type="none">
                  <lh ><b>OUR COMPANY&nbsp;&nbsp;</b></lh>
                  <li >About Us</li>
                  <li>Terms and Conditions</li>
                  <li >Privacy Policy</li>
              </ul>
          </div>
          <div class="col-sm-3"  id="socialmedia" >
              <ul type="none">
                  <lh><b>CONNECT WITH US</b></lh><br><br>
                  <a href=""><img src="https://img.icons8.com/fluent/96/000000/facebook-new.png"/></a>&nbsp;
                  <a href=""><img src="https://img.icons8.com/fluent/96/000000/twitter.png"  /></a>&nbsp;
                  <a href=""><img src="https://img.icons8.com/fluent/96/000000/linkedin.png" /></a>&nbsp;
                  <a href=""><img src="https://img.icons8.com/color/96/000000/instagram-new.png"  /></a>&nbsp;
                  <a href=""><img src="https://img.icons8.com/fluent/96/000000/telegram-app.png"/></a>
              </ul>
          </div>
          <div class="col-sm-3">
              <ul type="none">
                  <lh><b>KEEP UP TO DATE</b></lh>
                  <li>
                      <form style="display: inline;"><br>
                          <input type="email" placeholder="Email-Id" >&nbsp;
                          <input type="submit" value="Subscribe">
                      </form>
                  </li>
              </ul>
          </div>
         
  </div>
  <div class="row">
      <!-- <div class="col-sm-4">
          <ul type="none">
              
              <li>15 days return policy</li>
              <li>Return cash on Delivery</li>
              <li>Free Shipping above  &#8377; 500</li>
              
          </ul>
      </div> -->
      <!-- <div class="col-sm-4">
          <ul type="none">
              <lh ><b>Download the app now</b></lh><br>
              &nbsp;<a href=""><img src="./images/android.png" alt=""style="width: 80px;" ></a>&nbsp;
              <a href=""><img src="./images/ios.png" alt="" style="width: 80px;border:none;border-radius:3px ;"></a>
          </ul>
      </div> -->
      <div class="col-sm-3"></div>
      <div class="col-sm-3"></div>
  </div>
  <br>
  <hr style="height:1px;border:none;background-color:rgb(46, 44, 44);">
  <p style="text-align: center; color:white">&copy; 2021 Influencia.All rights reserved. </p><br>
  </div>
  
</footer>
</body>
<script type="text/javascript">
var chart = new CanvasJS.Chart("chartContainer1",
    {
        animationEnabled: true,
        zoomEnabled: true,
        zoomType: "xy",
        title: {
            text: "Top 5 tweets",
        },
        width: 600,
        toolTip:{
           content: "tweeted by: {label}" ,
        },
        axisX: {
            interval: 10,
            title: "Username"
        },
        axisY: {
          title: "Tweet influence index"
        },
        data: [
        {
            type: "column",
            // name: "Tweets",
            // color: "rgba(255,12,32,.3)",
            // showInLegend: true,
            dataPoints: <?php echo json_encode($data_points, JSON_NUMERIC_CHECK); ?>
        },
        ]
    });
chart.render();

// function onMouseover(e){
//   var val = e.dataPoint.y;
//   <?php 
//     $value = 'val';
//     echo($value);
//     $sql = "SELECT tweet FROM tweetcsv WHERE tii = '$value'";
//     $result = mysqli_query($sql);
//     print_r($result);
//   ?>
// }

// var chart = new CanvasJS.Chart("chartContainer2",
//     {
//         animationEnabled: true,
//         title: {
//             text: "Clustered Tweets",
//         },
//         data: [
//         {
//             type: "pie",
//             showInLegend: true,
//             dataPoints: <?php //echo json_encode($data_points3, JSON_NUMERIC_CHECK); ?>
//         },
//         ]
//     });
// chart.render();
// var chart = new CanvasJS.Chart("chartContainer3",
//     {
//         animationEnabled: true,
//         zoomEnabled: true,
//         zoomType: "xy",
//         title: {
//             text: "Top 5 users",
//         },
//         width: 600,
//         axisX: {
//             interval: 10,
//             title: "Username"
//         },
//         axisY: {
//           title: "General influence index",
//           //minimum: <?php //echo($res_min_arr[0]["min"]) ?>,
//           //maximum: <?php //echo($res_max_arr[0]["max"]) ?>

//         },
//         data: [
//         {
//             type: "column",
//             name: "users",
//             color: "rgba(255,12,32,.3)",
//             showInLegend: true,
//             dataPoints: <?php //echo json_encode($data_points1, JSON_NUMERIC_CHECK); ?>
//         },
//         ]
//     });
// chart.render();
var chart = new CanvasJS.Chart("chartContainer3",
    {
      title:{
        text: "Top 5 users"
      },
      legend: {
        horizontalAlign: "right",
        verticalAlign: "center"
      },
      toolTip:{
           content: "{legendText} : {y}" ,
        },
      width: 600,
      axisX: {
          interval: 10,
          title: "Username"
      },
      axisY: {
        title: "General influence index",
        //minimum: <?php //echo($res_min_arr[0]["min"]) ?>,
        //maximum: <?php //echo($res_max_arr[0]["max"]) ?>

      },
      data: [
      {
       type: "doughnut",
       name: "users",
        dataPoints: <?php echo json_encode($data_points1, JSON_NUMERIC_CHECK); ?>
      }
      ]
   });

    chart.render();
// var chart = new CanvasJS.Chart("chartContainer3",
//     {
//       title:{
//         text: "Top 5 users"
//       },
//       legend: {
//         horizontalAlign: "right",
//         verticalAlign: "center"
//       },
//       width: 600,
//       axisX: {
//           interval: 10,
//           title: "Username"
//       },
//       axisY: {
//         title: "General influence index",
//         //minimum: <?php //echo($res_min_arr[0]["min"]) ?>,
//         //maximum: <?php //echo($res_max_arr[0]["max"]) ?>

//       },
//       data: [
//       {
//         color: "LightSeaGreen",
//         indexLabelPlacement: "outside",
//         showInLegend: true,
//         type: "doughnut",
//         name: "users",
//         dataPoints: <?php //echo json_encode($data_points1, JSON_NUMERIC_CHECK); ?>
//       },
//       ]
//    });
//     chart.render();

var chart = new CanvasJS.Chart("chartContainer4",
    {
        animationEnabled: true,
        zoomEnabled: true,
        zoomType: "xy",
        title: {
            text: "Sentiment Analysis of Top 5 Tweets",
        },
        width: 600,
        axisX: {
            interval: 10,
            title: "Username"
        },
        axisY: {
          title: "Sentiment Polarity"
        },
        data: [
        {
            type: "column",
            // name: "tweets",
            // color: "rgba(255,12,32,.3)",
            // showInLegend: true,
            dataPoints: <?php echo json_encode($data_points2, JSON_NUMERIC_CHECK); ?>
        },
        ]
    });
chart.render();


</script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
setTimeout(function(){$('#loading').hide();$('.mymaindiv').show();}, 1000); 
  var preloader = document.getElementById("loading");
      function loader(){
        preloader.style.display = 'block';
      }
$(document).ready(function() {
    $('#example').DataTable();
} );
function logout(){
  <?php
    session_destroy();
  ?>
}
</script>
</html>