<!DOCTYPE html>
<html>
    <head>
        <title>Sign in</title>
        <!--Meta tag-->
        <link rel = "icon" href = "i.svg" type = "image/x-icon" width="200px">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Including the bootstrap CDN -->
        <link rel="stylesheet" href= "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> 
        <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
        <script src= "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"> </script> 
        <script src= "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> 
        <!--Including style sheet-->
        <link rel="stylesheet" href="style.css">
        <!--Google fonts-->
        <link href="https://fonts.googleapis.com/css2?family=Courgette&display=swap" rel="stylesheet">
       <!--Icon Of Page-->
             
 
    </head>
    <style>
        *::selection{
    background:rgb(149, 42, 42);
    color:white;
}
.wrapper {
  width: 100vw;
  height: 100vh;
  background: whitesmoke;
}
.flex-center {
  display: flex;
  justify-content: center;
  align-items: center;
}
.loadcontainer {
  width: 10em;
  height: 10em;
  position: relative;
  background: pink;
  border-radius: 50%;
}
.loadcontainer .dot {
  background: darkred;
  content: "";
  border-radius: 0.5em;
  margin-top: -0.5em;
  margin-left: auto;
  margin-right: auto;
  width: 1em;
  height: 1em;
  animation-name: bounce;
  animation-duration: 3000ms;
  animation-iteration-count: infinite;
  transition-timing-function: cubic-bezier(0, 0.99, 0, 0.1);
}
.container-dot {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
}
.dot-a {
  transform: rotate(0deg);
}
.dot-a .dot {
  animation-delay: 0ms;
}
.dot-b {
  transform: rotate(-22.5deg);
}
.dot-b .dot {
  animation-delay: -187.5ms;
}
.dot-c {
  transform: rotate(-45deg);
}
.dot-c .dot {
  animation-delay: -375ms;
}
.dot-d {
  transform: rotate(-67.5deg);
}
.dot-d .dot {
  animation-delay: -562.5ms;
}
.dot-e {
  transform: rotate(-90deg);
}
.dot-e .dot {
  animation-delay: -750ms;
}
.dot-f {
  transform: rotate(-112.5deg);
}
.dot-f .dot {
  animation-delay: -937.5ms;
}
.dot-g {
  transform: rotate(-135deg);
}
.dot-g .dot {
  animation-delay: -1125ms;
}
.dot-h {
  transform: rotate(-157.5deg);
}
.dot-h .dot {
  animation-delay: -1312.5ms;
}
@keyframes bounce {
  0%, 100% {
    transform: translatey(0);
 }
  50% {
    transform: translatey(10em);
 }
}

    </style>
    <body style="background-color: rgb(255, 242, 242); text-align: center;" onload="loader()">
    <div id="loading">
    <div class="wrapper flex-center">
        <div class="loadcontainer">
            <div class="container-dot dot-a">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-b">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-c">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-d">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-e">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-f">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-g">
            <div class="dot"></div>
            </div>
            <div class="container-dot dot-h">
            <div class="dot"></div>
            </div>
        </div>
    </div>
</div>
<div class="mymaindiv" style="display:none;">
<div class="loginclass">
            <br><br><br>
            <form method="POST" onsubmit="return validate_login()" action="login.php">
                <a href="index.php"><h1>Influencia</h1></a>
              
                <h3 style="color:  rgb(243, 148, 40);">SIGN IN!!</h3>
                <p style="color:  rgb(243, 148, 40);">Don't worry, we'd never share your data, or post anything on your behalf</p>
                <input type="text" name="fname" placeholder="First Name" class="myclas" id="1" required data-toggle="tooltip" data-placement="right" title="Enter The Correct Name" > &nbsp;
                <input type="text" name="lname" placeholder="Last Name" id="2" required  data-toggle="tooltip" data-placement="right" title="Enter The Correct Name" >
                <br><br>
                <input type="text" name="email"  data-toggle="tooltip" data-placement="right" title="Enter the your username from twitter account" placeholder="User Name" required>
                <br><br>
                <input type="password" name="password" placeholder="Password"  id="4" required  data-toggle="tooltip" data-placement="right" title="Password should be minimum 8 characters.It should contain one uppercase, one lowercase letter one special character and a number ">
                <br><br>
                <input type="tel" class="" name="contactno" placeholder="Contact Number" id="5" required  data-toggle="tooltip" data-placement="right" title="Enter the correct contact number. Add +91.">
                <br><br>
                <input type="checkbox" id="6" >&nbsp;<span id="7" data-toggle="tooltip" data-placement="right" title="Please tick the checkbox"><pre  style="padding-right:35px; font-family: inherit; display:inline">I have read and agree with the Terms & Conditions and Privacy Policy.</pre></span>
                <br><br>
                <input type="Submit"><br><br><br>
                <h5>Already have an account ? <a href="signin.php">Login</a></h5><br>
                
            </form>
        </div>
</div>
    </body>
    <!--Including our js file-->
    <script src="formvalidate.js"></script>
    <script>
    setTimeout(function(){$('#loading').hide();$('.mymaindiv').show();}, 1000); 
  var preloader = document.getElementById("loading");
      function loader(){
        preloader.style.display = 'block';
      }
</script>

</html>