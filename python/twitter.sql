-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2021 at 07:28 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `twitter`
--

-- --------------------------------------------------------

--
-- Table structure for table `hashtags`
--

CREATE TABLE `hashtags` (
  `id` int(10) NOT NULL,
  `hashtag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hashtags`
--

INSERT INTO `hashtags` (`id`, `hashtag`) VALUES
(1, '#covid'),
(2, '#covid');

-- --------------------------------------------------------

--
-- Table structure for table `tweetcsv`
--

CREATE TABLE `tweetcsv` (
  `id` int(10) NOT NULL,
  `tweet` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `sentiment` varchar(255) NOT NULL,
  `polarity` decimal(10,6) NOT NULL,
  `tii` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tweetcsv`
--

INSERT INTO `tweetcsv` (`id`, `tweet`, `user`, `sentiment`, `polarity`, `tii`) VALUES
(56, '@NationAfrica Mnaanza mbali na hapa  CBD #covid millionaires walipata tender wakiwa jail walking, sensing political targeting rather than the media doing their homework on all counties.', 'denzelshady', 'Negative', '-0.050000', '15450.980235'),
(57, '@CNN When #China hosted the Olympics the government had to train its citizens not to spit or blow their noses in public onto the sidewalks. I guess the millennial kids writing what they\'re told were too young #covid', 'Deplora70756691', 'Positive', '0.050000', '15522.898426'),
(58, 'Hi @sbg1 @kncukier Bolsonaro needs to be tried and arrested!  See the evidence of the crime👇Hello @CourPenaleInt @UN @DrTedros @DrMikeRyan @CDCgov @saude_pt @BorisJohnson @FLOTUS @POTUS @AOC @SpeakerPelosi @WHO @VP @hrw @mbachelet @wto\n#StopBolsonaro #gen', 'ThomasFreedomF1', 'Neutral', '0.000000', '15552.208267'),
(59, '#CORONAVIRUS #COVID-19 UPDATES\n\n4/12/2021 - Latest statistics and most complete testing data available \n\n60,000 #eBooks like #TheMarbleFaun,Vol.2 by #NathanielHawthorne - https://t.co/kzlgRg01Ib\n\nYou\'ll #love the #best trusted #free #web #resources\n\nhttps', 'kensbookinfo', 'Positive', '0.485714', '15703.555124'),
(60, 'The first thing I’m doing after I get fully vaccinated is lay next to my uncle in his hospital bed praying that he recovers from #COVID', 'TinaMaharath', 'Positive', '0.125000', '15728.323430');

-- --------------------------------------------------------

--
-- Table structure for table `usercsv`
--

CREATE TABLE `usercsv` (
  `id` int(10) NOT NULL,
  `user` varchar(255) NOT NULL,
  `follr_cnt` int(11) NOT NULL,
  `foll_cnt` int(11) NOT NULL,
  `likes_cnt` int(11) NOT NULL,
  `tweet_cnt` int(11) NOT NULL,
  `gii` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usercsv`
--

INSERT INTO `usercsv` (`id`, `user`, `follr_cnt`, `foll_cnt`, `likes_cnt`, `tweet_cnt`, `gii`) VALUES
(47, 'IndiaToday', 5587004, 252, 4437, 857532, '4701722603436.561000'),
(48, 'IndiaTodayFLASH', 1139096, 117, 135, 456294, '877463291763.282600'),
(49, 'FinancialXpress', 726658, 31, 21, 402593, '297012045441.998200'),
(50, 'Newsday', 292892, 701, 2215, 107411, '136003244494.155910'),
(51, 'IBMData', 202405, 121, 5885, 45041, '86599642281.117230');

-- --------------------------------------------------------

--
-- Table structure for table `user_acc`
--

CREATE TABLE `user_acc` (
  `id` int(10) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_acc`
--

INSERT INTO `user_acc` (`id`, `first_name`, `last_name`, `username`, `password`, `contact`) VALUES
(1, 'Vanshika', 'Bhavnani', 'vanshika123', 'vanshik', 2147483647),
(6, 'Gunjan', 'Bhawsinghka', 'GunjanBhawsing1', 'gunjanB123', 2147483647);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hashtags`
--
ALTER TABLE `hashtags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tweetcsv`
--
ALTER TABLE `tweetcsv`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usercsv`
--
ALTER TABLE `usercsv`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_acc`
--
ALTER TABLE `user_acc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hashtags`
--
ALTER TABLE `hashtags`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tweetcsv`
--
ALTER TABLE `tweetcsv`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `usercsv`
--
ALTER TABLE `usercsv`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `user_acc`
--
ALTER TABLE `user_acc`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
