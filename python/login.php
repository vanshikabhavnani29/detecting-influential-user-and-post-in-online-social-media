<?php
  
        session_start();
        // servername => localhost
        // username => root
        // password => empty
        // database name => staff
        $conn = mysqli_connect("localhost", "vanshika", "vanshik@143", "twitter");
          
        // Check connection
        if($conn === false){
            die("ERROR: Could not connect. " 
                . mysqli_connect_error());
        }
          
        // Taking all 5 values from the form data(input)
        if($_POST){
            $first_name =  $_POST['fname'];
            $last_name = $_POST['lname'];
            $email =  $_POST['email'];
            $pass = $_POST['password'];
            $contact = $_POST['contactno'];
            // $_SESSION["user"] = $email;
            // $_SESSION["fname"] = $first_name;
            // $_SESSION["lname"] = $last_name;  
            // Performing insert query execution
            // here our table name is college
            $sql = "INSERT INTO `user_acc`(first_name,last_name,username,password,contact)  VALUES ('$first_name', 
                '$last_name','$email','$pass','$contact')";
              
            $result = mysqli_query($conn,$sql);
            //$mysqli->close();
            if($result){
                echo("inserted successfully");
            }else{
                echo(mysqli_error($conn));
            }  
        }
        // Close connection
        mysqli_close($conn);
        ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <!--Meta tag-->
        <link rel = "icon" href = "i.svg" type = "image/x-icon" width="200px">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Including the bootstrap CDN -->
        <link rel="stylesheet" href= "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> 
        <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
        <script src= "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"> </script> 
        <script src= "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> 
        <!--Including style sheet-->
        <link rel="stylesheet" href="style.css">
        <!--Google fonts-->
        <link href="https://fonts.googleapis.com/css2?family=Courgette&display=swap" rel="stylesheet">
         
    </head>
    <body style="  background-color: rgb(255, 242, 242); text-align: center;">
    

        <div class="container-fluid signinclass">
            <br><br><br>
            <form method="POST" onsubmit="return validate_signin();" action="home.php">
                
                <a href="index.php"><h1 style="color: maroon;font-family: 'Courgette', cursive;font-size:xx-large; ">Influencia</h1></a>
                <h3 style="color:  rgb(243, 148, 40);">LOGIN</h3>
                
                <p style="color:rgb(243, 148, 40); margin-bottom: 25px; margin-top: 25px;">Don't worry, we'd never share your data, or post anything on your behalf</p>
                
                <input type="text"id="1" placeholder="User Name" required data-toggle="tooltip" data-placement="right" title="Enter the user name fro your twitter account" name="email"><br><br>
                <input type="password" id="2" placeholder="Password" required  data-toggle="tooltip" data-placement="right" name="password" title="Password should be minimum 8 characters.It should contain one uppercase, one lowercase letter one special character and a number "><br><br>
                <div class="showerror"></div>
                <input  type="submit" required><br><br>
                <h5>New here? <a href="signup.php">Sign up to get started!</a></h5>
               
                <br>
            </form><br>
        </div>     
    </body>
        <!--Including our js file-->
        <script src="signin.js"></script>
</html>
