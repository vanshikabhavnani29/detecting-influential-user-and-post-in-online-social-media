#!/usr/bin/env python
import tweepy
import webbrowser
import time
from IPython.display import display
import json
import datetime
import re
import mysql.connector as msql
from mysql.connector import Error
import random as rd
import pandas as pd
import numpy as np
from pandas import *
import math
import string
from difflib import SequenceMatcher
from textblob import TextBlob
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk
nltk.download('stopwords')

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

# authenticating twitter api credentials
consumer_key="vZ2iNo1EgsmscBHuXaRmFPygq"
consumer_secret="oCYlO7WDgUFIsDB95xqDvWGAdRyJEt0qhKakIgGVvk8qBUyHNl"
access_token="1272855765460480006-ia0Pcqc7JozAif9WAUu5twTKlht9iQ"
access_token_secret="PyU1tlbZgDkD5m6UBNk4fJjMUyBvOrr8ou3MDonG5ivIH"

# instantiating the api
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

# creating API object
api = tweepy.API(auth, wait_on_rate_limit=True)
#me=api.me()
#print(me.screen_name)
userID = ""
words = ""
try:
  conn = msql.connect(user='vanshika',  password='vanshik@143',host='localhost', database='twitter')
  if conn.is_connected():
    cursor = conn.cursor()        
    #sql = "SELECT username FROM user_acc ORDER BY id DESC LIMIT 1"
    query = "SELECT hashtag FROM hashtags ORDER by id DESC LIMIT 1"
    #cursor.execute(sql)
    #result = cursor.fetchone()
    #print(result[0])
    #userID = result[0]
    cursor.execute(query)
    res = cursor.fetchone()
    words = res[0]
    conn.commit()
except Error as e:
  print("Error while connecting to MySQL", e)
#extraction of network details

#getting following list
def remove(url):
  import os
  if os.path.exists(url):
    os.remove(url)
  else:
    print("file doesnot exist")

# function to display data of each tweet
def printtweetdata(n, ith_tweet):
    print()
    print(f"Tweet {n}:")
    print(f"Username:{ith_tweet[0]}")
    print(f"Description:{ith_tweet[1]}")
    print(f"Location:{ith_tweet[2]}")
    print(f"Following Count:{ith_tweet[3]}")
    print(f"Follower Count:{ith_tweet[4]}")
    print(f"Total Tweets:{ith_tweet[5]}")
    print(f"Retweet Count:{ith_tweet[6]}")
    print(f"Tweet Text:{ith_tweet[7]}")
    print(f"Hashtags Used:{ith_tweet[8]}")
    print(f"tweet created at:{ith_tweet[9]}")
  

   
def scrape(words):

    #print(words)
      
    # Creating DataFrame using pandas
    db = pd.DataFrame(columns=['name', 'User_Desc', 'User_Loc', 'Following_Count',
                               'Follower_Count', 'tweet_count', 'retweet_count', 'tweet', 'hashtags','tweet_created_at',
                               "user_craeted_at","Like_count",'Len_tweet','user_id','Liked_Tweet_Count','Profile_img_url'])
      
    # We are using .Cursor() to search through twitter for the required tweets.
    # The number of tweets can be restricted using .items(number of tweets)
    #until - 7 day limit
    #print(words)
    tweets = tweepy.Cursor(api.search, q= words+' -filter:retweets since:2021-1-1', lang="en",
                           tweet_mode='extended',wait_on_rate_limit=True,result_type='mixed').items(100)
     
    # .Cursor() returns an iterable object. Each item in 
    # the iterator has various attributes that you can access to 
    # get information about each tweet
    list_tweets = [tweettexts for tweettexts in tweets]
      
    # Counter to maintain Tweet Count
    i = 1  
      
    # we will iterate over each tweet in the list for extracting information about each tweet
    for tweettext in list_tweets:
        
        name = tweettext.user.screen_name
        User_Desc = tweettext.user.description
        User_Loc = str(tweettext.user.location)
        Following_Count = tweettext.user.friends_count
        Follower_Count = tweettext.user.followers_count
        tweet_count = tweettext.user.statuses_count
        retweet_count = tweettext.retweet_count
        hashtags = tweettext.entities['hashtags']
        tweet_created_at=tweettext.created_at
        user_craeted_at= tweettext.user.created_at   
        Like_count= tweettext.favorite_count
        Len_tweet=    len( tweettext.full_text.encode("utf-8").decode("utf-8"))
        user_id=  tweettext. user.id
        Liked_Tweet_Count=  tweettext.user.favourites_count
        Profile_img_url=   tweettext.user.profile_image_url_https
          
        # Retweets can be distinguished by a retweeted_status attribute,
        # in case it is an invalid reference, except block will be executed
        try:
            tweet = tweettext.retweeted_status.full_text
        except AttributeError:
            tweet = tweettext.full_text
        hashtext = list()
        for j in range(0, len(hashtags)):
            hashtext.append(hashtags[j]['text'])
          
        # Here we are appending all the extracted information in the DataFrame
        ith_tweet = [name, User_Desc, User_Loc, Following_Count,
                     Follower_Count, tweet_count, retweet_count, tweet, hashtext,
                     tweet_created_at,user_craeted_at,Like_count,Len_tweet,user_id,Liked_Tweet_Count,Profile_img_url]
        db.loc[len(db)] = ith_tweet
          
        # Function call to print tweet data on screen
        #printtweetdata(i, ith_tweet)
        #i = i+1
    userID=words[1:]
    print(userID)
    filename = userID+'_tweets.csv'
    print(filename)
    # we will save our database as a CSV file.
    remove(filename)
    db.to_csv(filename,index=False)
    people2=[]
    people2=db['name'].tolist()
    #print(len(people2))
    people2 = list(dict.fromkeys(people2))
    #print(len(people2))
    return userID,people2

  
# Enter Hashtag cmd
# number of tweets you want to extract in one run
#numtweet = 100  
userID,people2=scrape(words)
print('Scraping has completed!')
print(userID)
#print(people2)

#preprocessing on extracted csv n storing in csv without nan values and details of tweets
#arg pass -hashtag, username

def processing(url):
    print(url)
    

    #url=userID+'_tweets.csv'

    dff=read_csv(url)
    #display(dff)
    tweets=dff['tweet'].tolist()
    #print(type(tweets[0]))
    counthsh,counturl,countmen,counturl1,counturl2=0,0,0,0,0
    j=0
    k=0
    for i in tweets:
        x=re.findall("#",(i))#list of hash
        y=re.findall("((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))",(i))#count of links
        a=re.findall("@",i)#count of mentions
        counthsh=len(x)
        dff.at[j, 'hs_cnt'] = counthsh
        counturl=len(y)
        dff.at[j, 'url_cnt'] = counturl#+counturl1+counturl2
        countmen=len(a)
        dff.at[j,'men_cnt']=countmen  
        #lower text-
        # remove \n from the end after every sentence
        i = i.strip('\n')

        # Remove the tweet id and timestamp
        #i = i[50:]

        #remove mentions
        i=re.sub(r'@\S+','',i)
      
        #remove links
        i=re.sub("((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))",'',i)

        #remove hashtags
        i=re.sub(r'#\S+','',i)

        #remove emojis and punctuations (only keep alphabets,digits,spaces between words)
        i = re.sub('[^A-Za-z0-9]+', ' ', i)

        # remove colons from the end of the sentences (if any) after removing url
        i = i.strip()
        tweet_len = len(i)
        if tweet_len > 0:
            if i[len(i) - 1] == ':':
                i = i[:len(i) - 1]

        #lower text-
        i=i.lower()

        #remove retweets
        i=re.sub(r'RT\S+','',i)

        # remove punctuations
        i = i.translate(str.maketrans('', '', string.punctuation))

        # trim extra spaces
        i = " ".join(i.split())  
        #print(i)
        if(i != ' ' and i!= ''):
          dff.at[j,'new']=i    #new col in extracted csv with nan value 
        #dff me uss i ke saare detisl hai
        j+=1
    display(dff)

    #csv of processed not nan tweets
    newdff=DataFrame()

    newlist=dff['new'].tolist()
    #print(newlist[5])
    name=dff['name'].tolist()
    tweet_Count = dff['tweet_count'].tolist()
    Tweet = dff['tweet'].tolist()
    hshcnt = dff['hs_cnt'].tolist()
    tweet_count = dff['tweet_count'].tolist()
    Follower_Count = dff['Follower_Count'].tolist()
    Following_Count = dff['Following_Count'].tolist()
    Profile_img_url = dff['Profile_img_url'].tolist()
    Len_tweet = dff['Len_tweet'].tolist()
    retweet_count = dff['retweet_count'].tolist()
    url_cnt = dff['url_cnt'].tolist()
    men_cnt = dff['men_cnt'].tolist()
    Like_count = dff['Like_count'].tolist()
    Liked_Tweet_Count = dff['Liked_Tweet_Count'].tolist()
    Tweet_createdat = dff['tweet_created_at'].tolist()
    #print(len(newlist))
   
    j=0
    for i in range(len(newlist)):

      #print(i)
      if(pd.isnull(newlist[i]) == False):

        #print(i)
        newdff.at[j,'name']=name[i]
        newdff.at[j,'tweet_count']=tweet_Count[i]
        newdff.at[j,'tweets']=newlist[i]
        newdff.at[j,'Original_Tweet']=Tweet[i]
        newdff.at[j,'hs_cnt']=hshcnt[i]
        newdff.at[j,'Tweet_Count']=tweet_count[i]
        newdff.at[j,'Follower_Count']=Follower_Count[i]
        newdff.at[j,'Following_Count']=Following_Count[i]
        newdff.at[j,'Profile_img_url']=Profile_img_url[i]
        newdff.at[j,'Len_tweet']=Len_tweet[i]
        newdff.at[j,'retweet_count']=retweet_count[i]
        newdff.at[j,'url_cnt']=url_cnt[i]
        newdff.at[j,'men_cnt']=men_cnt[i]
        newdff.at[j,'Like_count']=Like_count[i]
        newdff.at[j,'Liked_Tweet_Count']=Liked_Tweet_Count[i]
        newdff.at[j,'Tweet_created_at']=Tweet_createdat[i]
      j+=1
    
    display(newdff)


    remove(url[:-11]+"_processedtweets.csv")
    newdff.to_csv('%s_processedtweets.csv' % url[:-11],index=False)


processing(userID+'_tweets.csv')


#weights calc func for criterias-static

import sys
#from AHP import *
class AHP:
    def __init__(self, criFile):
        #print(criFile)
        with open(criFile, 'r') as f: 
            self.CriList=[]
            self.criList = f.readlines()#each feature name
           # print(self.criList)
            self.criCount = len(self.criList) #count of total featuresin the file
            
            for i in self.criList:
              
              j=i[0:-2]
              self.CriList.append(j)


        if self.criCount == 8:
            #user

            self.compMat =[[(1,1,1),(1/7,1/6,1/5),(1/9,1/8,1/7),(1/4,1/3,1/2),(1/7,1/6,1/5),(2,3,4),(2,3,4),(1,2,3)],
                            [(5,6,7),(1,1,1),(1/4,1/3,1/2),(2,3,4),(1/3,1/2,1),(5,6,7),(5,6,7),(3,4,5)],
                            [(7,8,9),(2,3,4),(1,1,1),(6,7,8),(1,2,3),(7,8,9),(7,8,9),(5,6,7)],
                            [(2,3,4),(1/4,1/3,1/2),(1/8,1/7,1/6),(1,1,1),(1/4,1/3,1/2),(2,3,4),(2,3,4),(1,2,3)],
                            [(5,6,7),(1,2,3),(1/3,1/2,1),(2,3,4),(1,1,1),(6,7,8),(6,7,8),(4,5,6)],
                            [(1/4,1/3,1/2),(1/7,1/6,1/5),(1/9,1/8,1/7),(1/4,1/3,1/2),(1/8,1/7,1/6),(1,1,1),(1,1,1),(1/5,1/4,1/3)],
                            [(1/4,1/3,1/2),(1/7,1/6,1/5),(1/9,1/8,1/7),(1/4,1/3,1/2),(1/8,1/7,1/6),(1,1,1),(1,1,1),(1/8,1/7,1/6)],
                            [(1/3,1/2,1),(1/5,1/4,1/3),(1/7,1/6,1/5),(1/3,1/2,1),(1/6,1/5,1/4),(3,4,5),(6,7,8),(1,1,1)]]
           # print(self.compMat[7][7][2])
        else:
            #tweet
            self.compMat =[[(1,1,1),(1/9,1/8,1/7),(1/10,1/9,1/8),(1/3,1/2,1),(1/4,1/3,1/2),(1/4,1/3,1/2)],
        [(7,8,9),(1,1,1),(1/3,1/2,1),(7,8,9),(7,8,9),(7,8,9)],
        [(8,9,10),(1,2,3),(1,1,1),(8,9,10),(8,9,10),(8,9,10)],
        [(1,2,3),(1/9,1/8,1/7),(1/10,1/9,1/8),(1,1,1),(1,2,3),(1,2,3)],
        [(2,3,4),(1/9,1/8,1/7),(1/10,1/9,1/8),(1/3,1/2,1),(1,1,1),(1/3,1/2,1)],
       [(2,3,4),(1/9,1/8,1/7),(1/10,1/9,1/8),(1/3,1/2,1),(1,2,3),(1,1,1)]]
      
       
        
        #self.criCount=len(criFile)
        #print(self.criCount)
  
      
def runtu(filename):

    session = AHP(filename+'.txt')

    N = session.criCount  #6/8 for us
 
  
    #print("Criteria = " + str(N))



    #print('Fuzzy geometric mean for each row:')
    r = []
    for x in range(N):

        p1, p2, p3 = 1, 1, 1
        for y in range(N):

            p1, p2, p3 = p1 * session.compMat[x][y][0], p2 * session.compMat[x][y][1], p3 * session.compMat[x][y][2]
        p1, p2, p3 = p1**(1/N), p2**(1/N), p3**(1/N)
        r.append((p1, p2, p3))
        #print(session.criList[x] + " = " + str(r[x]))


    #print('Relative fuzzy weight for each criterion:')
    f = []
    l, m, u = 0, 0, 0
    for i in range(N):
        l, m, u = l + r[i][0], m + r[i][1], u + r[i][2]
    for i in range(N):
        f.append((r[i][0] / u, r[i][1] / m, r[i][2] / l))
        #print(session.criList[i] + " = " + str(f[i]))
   

    #print('Defuzzification by COA method:')
    d = []
    for i in range(N):
        d.append((f[i][0] + f[i][1] + f[i][2]) / 3)
    # for i in range(N):
    #     print(session.criList[i] + " = " + str(d[i]))
   

    #print('Normalized weights for each criterion:')
    w = []
    s = 0
    for i in range(N):
        s = s + d[i]
    for i in range(N):
        w.append(d[i] / s)
    # for i in range(N):
    #     print(session.criList[i] + " = " + str(w[i]))
   

    #storing in resp var the weights
    t={}
    for i in range(N):
      #print(i)

      t[session.CriList[i]]=w[i]
    #print(t)
    return t


t={}#storing weights of tweet features in dict
u={}#storing weights of user features in dict
crit="crit"
cri="cri"
#print(len(crit+'.txt')) 
u=runtu(cri)
t=runtu(crit)
print("tweet features weights",t)
print("user features weights",u)

#tii calc for all tweets
#calculation of tii and uii


#calc time n store in list t
def time(url):
  #url = userID+'_processedtweets.csv'
  #print(url)
  data = read_csv(url)
  #display(data)
  tweet = data['tweets'].tolist()
  tweetcreatedat = data['Tweet_created_at'].tolist()
  #print(tweet)
  #print((tweetcreatedat[0]))#list of dates
  tweetdate=[]#list with only dates of tweets
  for i in (tweetcreatedat):
    j=i[0:-9]
    tweetdate.append(j)
  #print(tweetdate)

  timelist=[]
  #j=0
  for i in range(len(tweet)):
     
      a=tweetdate[i][0:4]
      b=tweetdate[i][5:7]
      c=tweetdate[i][8:10]

      today=datetime.date.today()
      # print(today)
      # print(a,b,c)
      
      mnew=datetime.date(int(a),int(b),int(c))
      # print(mnew)

      today=datetime.date.today()

      difff=today-mnew
      # print(difff)
      total =difff.days
      # print(total)
      x=(total)+1
      # print(x)
      time=(1/x)
      # print(time)
      timelist.append(time)
    
      
  # print(len(t))
  return timelist

def cosinesim(a,b):
  
  
  return SequenceMatcher(None,a,b).ratio()



def sim(url):
  #url = userID+'_processedtweets.csv'
  print(url)
  data = read_csv(url)
  display(data)
  tweets = data['tweets'].tolist()
  costotal=0
  cosinelist=[]
#take one tweet, calc simof that tweet with other tweets n sum it for that tweet sim value
  for i in range(len(tweets)):

      X=tweets[i]
      
      for j in range(len(tweets)):
        if i!=j :
          Y=tweets[j]
          cosvalue=cosinesim(X,Y)
          costotal+=cosvalue #sim of one tweet
      cosinelist.append(costotal)#list with each tweets total sim value
  #print(cosinelist)
  
          



  return cosinelist

#calc tii for all tweets present in list_of_tweets of preprocessing func--done
#instead of minn take list_of_tweets--done
#and after tii , map clustered tweets' tweet to this tweets and take resp tii for infl tweet 

def formulatweet(t,url):
  #url = userID+'_processedtweets.csv'
  print(url)
  data = read_csv(url)
  display(data)
#for tsii
  hshcnt = data['hs_cnt'].tolist()
  Len_tweet = data['Len_tweet'].tolist()
  retweet_count = data['retweet_count'].tolist()
  url_cnt = data['url_cnt'].tolist()
  men_cnt = data['men_cnt'].tolist()
  Like_count = data['Like_count'].tolist()
  tweet = data['tweets'].tolist()
  tweetcreatedat = data['Tweet_created_at'].tolist()
  #print(len(tweet))
  #print(t)
  #print(t['hashCnt'])
  
#######################  sim not have
  #j=0
  time1=[]
  tii=[]
  time1=time(url)
  print(len(time1))
  sim1=sim(url)
  print(len(sim1))
  #print(time1)
  j=0
  for i in range(len(tweet)):

    TSII=(t['hashCnt'] * hshcnt[i] )+(t['len'] * Len_tweet[i]) -(t['menCnt'] * men_cnt[i]) -(t['urlC'] * url_cnt[i])
    #print(TSII)
    #data.at[j,'TSII']=TSII

    
    #print(retweet_count)
    TDII=((sim1[i]+ t['retweetCn']*retweet_count[i])+(t['tweetFavCnt']*Like_count[i]))*time1[i]
    #print(TDII)
    #data.at[j,'TDII']=TDII


    TII=TSII+TDII
    tii.append(TII)
    print(TII)
    
    #making other df with tii col

    
    data.at[j,'TII']=TII   #new col in extracted csv with nan value 
        #dff me uss i ke saare detisl hai
    j+=1
    
  print("tii",tii)
  display(data)
  remove(url[:-20]+"_processedtweets_tii.csv")
  data.to_csv('%s_processedtweets_tii.csv' % url[:-20],index=False)
  return tii


#tii for userID_processed- for infl tweet
ttii=formulatweet(t,userID+'_processedtweets.csv')
print("tii for tweet",ttii)
print((ttii))


#infl user n tweet detection

#infl tweet 
def gettweet(maxtii,tiilist):
  data_url = userID+'_processedtweets_tii.csv'
  df = read_csv(data_url)
  postii=tiilist.index(maxtii)#index of that tii
  maxtweet=df['Original_Tweet'][postii]#retreiving origianl tweet of that tii
  #print(maxtweet)
  maxuser=df['name'][postii]
  #print(maxuser)
  

  return maxtweet,maxuser

def infltweet():
    
  data_url = userID+'_processedtweets_tii.csv'
  df = read_csv(data_url)
 
  tiilist=df['TII'].tolist()
  maxtii=max(tiilist)
  #print(maxtii)
  #print(maxtii)#max tii value from list
  maxtweet,maxuser=gettweet(maxtii,tiilist)#infl tweet text
  #print(maxtweet)
  #get top 5 tii values
  print(tiilist)
  tiilistcpy=tiilist.copy()#unchanged modification
  print("cpy of orig ",tiilistcpy)
#tiilistcpy contains orig tii values in seq with tweets in .csv

  tiilist.sort(reverse = True)
  print("reversed tii list",tiilist)
  top5tweetvalues=[]
  top5tweetvalues.append(tiilist[0])#store max tii value
  m=1
  count=1

  while count < 5:
    m1=tiilist[m]#start from second max 
    if m1!=top5tweetvalues[-1]:
      top5tweetvalues.append(m1)
      m=m+1
      count=count+1
    else:
      m=m+1
      continue
  print("top tweets",top5tweetvalues)
     
  top5tweet=[]
  top5tweetuser=[]
  for i in top5tweetvalues:
    tweet,tweetuser=gettweet(i,tiilistcpy)
    top5tweet.append(tweet)
    top5tweetuser.append(tweetuser)
   


  #print(top5)
  return maxtweet,maxuser,top5tweet,top5tweetuser,top5tweetvalues

#infl tweet
maxtweet,maxuser,toptweets,toptweetsuser,top_2_values=infltweet()#infl tweet
#toptweets=infltweet()[1]#top5  infl tweets list
print("infl tweet:\n",maxtweet,"by\n",maxuser)
print("top 5 tweets:",toptweets,"by \n",toptweetsuser)
print("top 5 tweets' tii values:\n",top_2_values)



#extracting user tweets 

number_of_tweets=200
def tweetextract(i):
    user=api.get_user(i)
    all_tweets=[]
  
    tweets = api.user_timeline(screen_name=i, 
                                  # 200 is the maximum allowed count
                                  count=200,
                                  include_rts = False,
                                  # Necessary to keep full_text 
                                  # otherwise only the first 140 words are extracted
                                  tweet_mode = 'extended'
                                  )
    #print(tweets)
    #print(len(tweets))
    all_tweets.extend(tweets)
    outtweets = [[
                    user.created_at,  
                  tweet.id_str, 
                  tweet.created_at, 
                  tweet.favorite_count, 
                  tweet.retweet_count, 
                  tweet.full_text.encode("utf-8").decode("utf-8"),
                     tweet.entities['hashtags'],
                    len( tweet.full_text.encode("utf-8").decode("utf-8")),
                  user.id,
                  user.screen_name,
                  user.statuses_count,
                  user.favourites_count,
                  user.followers_count,
                  user.friends_count,
                str(user.location),
                user.description,
                user.profile_image_url_https
               
                 ] 
                 for idx,tweet in enumerate(all_tweets)]
    df = DataFrame(outtweets,columns=["user_craeted_at","tweet_id","tweet_created_at","Like_count",
                                      "retweet_count", "tweet","hashtags","Len_tweet","user_id","name",
                                      "tweet_count","Liked_Tweet_Count","Follower_Count","Following_Count",
                                      "User_Loc","User_Desc","Profile_img_url"])
    #print("in function")
  
    display(df)
    return df

dff=DataFrame()
print(toptweetsuser)
for i in toptweetsuser:
  print("i",i)
  try:
    userid=api.get_user(i)
    if str(userid.protected)=="False":
      df1=tweetextract(i)
      dff=dff.append(df1, ignore_index = True) 
  except tweepy.TweepError as e:
    print("Going to sleep:", e)
    time.sleep(5)
print("dff extracted")
remove(userID+'_users123_tweets.csv')
dff.to_csv(userID+'_users123_tweets.csv' ,index=False)

#process users tweet extracted
processing(userID+'_users123_tweets.csv')


  
#calc of uii for all users of network
  
#calc of uii for all users of network

def formulauser(people2,u,data_url):
  
  
  uii=[]
  #print(people2)

  #data_url = userID+'_tweets.csv'
  print(data_url)
  dataurl= read_csv(data_url)
  print(userID)
  #people2.append(userID)####wrong --- run for userid seperately
  print(people2)

  for i in people2:
    data=dataurl[dataurl['name']==i]
    display(data)
    #list count
    user=api.get_user(i)
    listcnt=user.listed_count
   # print("listcnt",listcnt)
    tweet_cnt = data['tweet_count'].tolist()
    #print(tweet_cnt)
    #print(tweet_cnt[0])
    userfav_cnt=data['Liked_Tweet_Count'].tolist()
    fler_cnt = data['Follower_Count'].tolist()
    fling_cnt = data['Following_Count'].tolist()
    img_url = data['Profile_img_url'].tolist()
    description = data['User_Desc'].tolist()
    userLoc = data['User_Loc'].tolist()
   

    
    tweetcnt = tweet_cnt[0]
    userfavcnt = userfav_cnt[0]
    flercnt=fler_cnt[0]
    flingcnt=fling_cnt[0]
    des  = description[0]
    imgurl=img_url[0]
    userloc=userLoc[0]
    #print(userloc)
    #print(i,tweetcnt)
    if pd.isnull(userloc) == False:
      userloc=1
    else:
      userloc=0
    #print(userloc)
    if pd.isnull(imgurl) == False:
      imgurl=1
    else:
      imgurl=0
    #print(imgurl)

    if pd.isnull(des) == False:
      des=1
    else:
      des=0
    #print(des)

    UII=(flercnt * u['flerCnt'] )+(listcnt * u['listCn'])+(des*u['des'])+(tweetcnt * u['tweetCn'])-(flingcnt* u['flingCnt'])-(userfavcnt * u['userFavCnt'])+(userloc * u['loc'])+(imgurl * u['imgU'])
    
    uii.append(UII)
  print("uii",uii)
  return uii


#uii for userID_tweet-for infl user
uii=formulauser(toptweetsuser,u,userID+'_users123_tweets.csv')



#tii for userID_user123_processed- for infl user

utii=formulatweet(t,userID+'_users123_processedtweets.csv')
print("tii for user tweets",utii)

#calc gii for each user
#we have calc tii for only clustered tweets which is used for tweet detection
#for user detec we have user uii * that user's all tweets' tii 
#processedtweets me se user select , match it from tii by pos

def formulagii(uii,people2,data_url):
  print(uii)
 

  #data_url = userID+'_processedtweets.csv'
  df = read_csv(data_url)
  utii=df['TII'].tolist()
  print(utii)
  #print((uii))
  #print((tii))
  #print(people2)
  gii={}
  tiivalue=0

  for i in range(len(people2)):
    newdf=df[df['name']==people2[i]]
    display(newdf)
    tweets=newdf['tweets'].tolist()
    dftweets=df['tweets'].tolist()
    #print(len(dftweets))
    for k in tweets:
      #print(k)
      if k in dftweets:
         pos= dftweets.index(k) 
         #print("pos",pos)
         tiivalue+=utii[pos]
         #print(tiivalue)

    #print(tiivalue)
    GII=uii[i] * tiivalue
    #print(GII)

    gii[people2[i]]=GII
  print(gii)
  return gii

gii=formulagii(uii,toptweetsuser,userID+'_users123_processedtweets_tii.csv')



def influser(gii):
  #infl user from max gii value 
  maxgii=max(gii, key=gii.get)
  x=sorted(gii, key=gii.get, reverse=True)
  #print(x)
  #print(x[:5])
  topusers=x[:5]
  topusersgii=[]
  for i in topusers:
    topusersgii.append(gii[i])
  #print(topusersgii)


  followercnt1=[]
  followingcnt1=[]
  likestweetscnt1=[]
  tweetcnt1=[]
  data_url = userID+'_users123_processedtweets.csv'
  df = read_csv(data_url)
  
  for i in topusers:
    print(i)
    newdf=df[df['name']==i]
    #display(newdf)
    followingcntlist=newdf['Following_Count'].tolist()
    likedtweetcntlist=newdf['Liked_Tweet_Count'].tolist()
    tweetcntlist=newdf['tweet_count'].tolist()
    followercntlist=newdf['Follower_Count'].tolist()

   
    
    if len(followercntlist)==0:
      followercnt=0
    else:
      followercnt=followercntlist[0]

    followercnt1.append(followercnt)

    if len(followingcntlist)==0:
      followingcnt=0
    else:
      followingcnt=followingcntlist[0]


    followingcnt1.append(followingcnt)

    if len(likedtweetcntlist)==0:
      likestweetscnt=0
    else:

      likestweetscnt=likedtweetcntlist[0]
    likestweetscnt1.append(likestweetscnt)

    if len(tweetcntlist)==0:
      tweetcnt=0
    else:
      tweetcnt=tweetcntlist[0]

      tweetcnt1.append(tweetcnt)

    
    
    
 # print(followercnt,followingcnt,likestweetscnt,tweetcnt)


  return maxgii,topusers,followercnt1,followingcnt1,likestweetscnt1,tweetcnt1,topusersgii


#infl user 
maxgii,topusers,followercnt1,followingcnt1,likestweetscnt1,tweetcnt1,topusersgii=influser(gii)
print("infl user:\n",maxgii)#infl user 
print("top 5 infl user:\n",topusers)
print("top 5 users' gii value\n:",topusersgii)
print("follower list of top 5:\n",followercnt1)
print("following list of top 5:\n",followingcnt1)
print("liked tweet cnt list of top 5:\n",likestweetscnt1)
print("tweet cnt list of top 5:\n",tweetcnt1)

#trad.py



import pandas as pd
import numpy as np

# text preprocessing
from nltk import word_tokenize
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
import re

# plots and metrics
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix

# feature extraction / vectorization
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

# classifiers
# from sklearn.naive_bayes import MultinomialNB
# from sklearn.linear_model import LogisticRegression, SGDClassifier
# from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline

# save and load a file
import pickle




df_train = pd.read_csv('data_train.csv')
df_test = pd.read_csv('data_test.csv')

X_train = df_train.Text
X_test = df_test.Text

y_train = df_train.Emotion
y_test = df_test.Emotion

class_names = ['joy', 'sadness', 'anger', 'neutral', 'fear']
data = pd.concat([df_train, df_test])

print('size of training set: %s' % (len(df_train['Text'])))
print('size of validation set: %s' % (len(df_test['Text'])))
print(data.Emotion.value_counts())

data.head()

# def plot_confusion_matrix(y_true, y_pred, classes,
#                           normalize=False,
#                           title=None,
#                           cmap=plt.cm.Blues):
#     '''
#     This function prints and plots the confusion matrix.
#     Normalization can be applied by setting `normalize=True`.
#     '''
#     if not title:
#         if normalize:
#             title = 'Normalized confusion matrix'
#         else:
#             title = 'Confusion matrix, without normalization'

#     # Compute confusion matrix
#     cm = confusion_matrix(y_true, y_pred)

#     if normalize:
#         cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

#     fig, ax = plt.subplots()
    
#     # Set size
#     fig.set_size_inches(12.5, 7.5)
#     im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
#     ax.figure.colorbar(im, ax=ax)
#     ax.grid(False)
    
#     # We want to show all ticks...
#     ax.set(xticks=np.arange(cm.shape[1]),
#            yticks=np.arange(cm.shape[0]),
#            # ... and label them with the respective list entries
#            xticklabels=classes, yticklabels=classes,
#            title=title,
#            ylabel='True label',
#            xlabel='Predicted label')

#     # Rotate the tick labels and set their alignment.
#     plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
#              rotation_mode="anchor")

#     # Loop over data dimensions and create text annotations.
#     fmt = '.2f' if normalize else 'd'
#     thresh = cm.max() / 2.
#     for i in range(cm.shape[0]):
#         for j in range(cm.shape[1]):
#             ax.text(j, i, format(cm[i, j], fmt),
#                     ha="center", va="center",
#                     color="white" if cm[i, j] > thresh else "black")
#     fig.tight_layout()
#     return ax


def preprocess_and_tokenize(data):    

    #remove html markup
    data = re.sub("(<.*?>)", "", data)

    #remove urls
    data = re.sub(r'http\S+', '', data)
    
    #remove hashtags and @names
    data= re.sub(r"(#[\d\w\.]+)", '', data)
    data= re.sub(r"(@[\d\w\.]+)", '', data)

    #remove punctuation and non-ascii digits
    data = re.sub("(\\W|\\d)", " ", data)
    
    #remove whitespace
    data = data.strip()
    
    # tokenization with nltk
    data = word_tokenize(data)
    
    # stemming with nltk
    porter = PorterStemmer()
    stem_data = [porter.stem(word) for word in data]
        
    return stem_data



import nltk
nltk.download('punkt')
# TFIDF, unigrams and bigrams
vect = TfidfVectorizer(tokenizer=preprocess_and_tokenize, sublinear_tf=True, norm='l2', ngram_range=(1, 2))

# fit on our complete corpus
vect.fit_transform(data.Text)

# transform testing and training datasets to vectors
X_train_vect = vect.transform(X_train)
X_test_vect = vect.transform(X_test)




svc = LinearSVC(tol=1e-05)
svc.fit(X_train_vect, y_train)

ysvm_pred = svc.predict(X_test_vect)

# print("Accuracy: {:.2f}%".format(accuracy_score(y_test, ysvm_pred) * 100))
# print("\nF1 Score: {:.2f}".format(f1_score(y_test, ysvm_pred, average='micro') * 100))
# print("\nCOnfusion Matrix:\n", confusion_matrix(y_test, ysvm_pred))

# plot_confusion_matrix(y_test, ysvm_pred, classes=class_names, normalize=True, title='Normalized confusion matrix')
# plt.show()



#Create pipeline with our tf-idf vectorizer and LinearSVC model
svm_model = Pipeline([
    ('tfidf', vect),
    ('clf', svc),
])




# save the model
filename = 'tfidf_svm.sav'
pickle.dump(svm_model, open(filename, 'wb'))


model = pickle.load(open(filename, 'rb'))


###senti 

#toptweets=['Hope your day was awesome! This is just me handling a #zoom call and drinking some #nespresso. #kerimaletto #coffee #spring #hello https://t.co/FNNspLSdFU', 'There, my classmates are great and funny. I enjoyed my times for 1 semester and even found myself a partner in my class. Cute, sweet. My gang and 2 bestfriends; rough, funny and sweet.\n\n-To be continued-\n#shortstory \n#hello \n#thanks', 'The Return! https://t.co/SG6NeBaHjs\n#twitch\n#comewatch \n#doctor\n#pc\n#gaming \n#itstime\n#return\n#Hello! \n#gamer \n#yay', "Sorry for the inconvenience. #payphone #alsoasis #westriver #bisonburger #hello #tourism #southdakota @ Al's Oasis https://t.co/16qVGEWmkT", '@ollielupinn #hello I want']

print(toptweets)
encoding = {
    'joy': 0,
    'fear': 1,
    'anger': 2,
    'sadness': 3,
    'neutral': 4
}

sentilist=[]
sentlist=[]
sentivalue=[]
for i in range(len(toptweets)): 
   

    predictt=model.predict([toptweets[i]])
    
  
    sentlist.append(predictt)
    a=sentlist[i]
    a=a.tolist()
  
    a=a[0]
    sentilist.append(a)
   
    sentivalue.append(encoding[a])
   
print("sentilist",sentilist)
print("sentivalue",sentivalue)

df_top_5_users = pd.DataFrame(list(zip(topusers,followercnt1,followingcnt1,likestweetscnt1,tweetcnt1,topusersgii)),columns=['user','followercnt','followingcnt','likestweetscnt','tweetcnt','gii'])
df_top_5_tweets = pd.DataFrame(list(zip(toptweets, toptweetsuser,sentilist,sentivalue,top_2_values)),columns=['tweets','user','sentiment','polarity','tii'])
remove("top_5_tweets.csv")
remove("top_5_users.csv")
df_top_5_tweets.to_csv('top_5_tweets.csv',index=False)
df_top_5_users.to_csv('top_5_users.csv',index=False)

#userID_tweets.csv-----extracted data
#userID_processedtweets.csv-----processed (not nan) tweets with their details----imp for gii calc
#people2----list of all users of network--imp for uii calc
#cri.txt
#crit.txt
#tii---tii list of all not nan tweets
#uii----uii list of all uers
#min1----least value of cluster sse
#min1_tweets.csv----clustered tweets 
#min1_features.csv----containing all details including TII of clustered tweets 
#gii-----dictionary with user key and his gii value 
#maxgii------infl user name 
#maxtweet-----infl tweet text
#maxuser-----infl tweet text's username
#toptweets-----top 5 infl tweets text in list
#toptweetsuser----top 5 infl tweets texts' usernames in list
#sentilist----list of pos,neg,neutral words acc to tweet from min1_features.csv

#infl user ->...from gii dict...pending...value of maxgii
#infl tweet ->...from min1_features.csv(TII col)...pending...maxtweet

#####code to delete files


#first check logout kiya na

# import os
# os.remove("demofile.txt")
# #code to check if file exists then delete it
